import React from 'react';

const userOutput = (props) => {
    const style = {
        "width": "60%",
        "padding": "16px",
        "margin": "16px",
        "border": "2px solid white",
        "text-align": "center"
    };

    return (
    <div className="form-control" style={style}>
        <p>{props.name === '' ? 'Hello World' : `Hello ${props.name}`}</p>
        <p>Cupidatat laborum dolore duis ipsum non ea. Tempor consequat amet culpa excepteur nostrud do excepteur occaecat aliquip velit ad. Labore dolore veniam fugiat adipisicing veniam ullamco ex laboris adipisicing ex. Do velit dolor nulla sunt mollit dolor consectetur. Consectetur consectetur exercitation laboris ex tempor commodo et ut enim magna est sit incididunt. Culpa nostrud enim commodo nulla eiusmod est fugiat cupidatat sint duis. Qui veniam cillum voluptate esse incididunt ut eiusmod qui est mollit magna aliqua adipisicing.</p>
    </div>
    )
}

export default userOutput;